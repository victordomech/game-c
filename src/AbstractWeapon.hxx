#ifndef AbstractWeapon_hxx
#define AbstractWeapon_hxx

//#include <string>
class Character;


class AbstractWeapon{
	public:
		virtual ~AbstractWeapon(){}
		
		virtual std::string hit(Character & cha) =0;
		
		//virtual string hit (Character &cha) = 0;
		
};

#endif
