#ifndef Trap_hxx
#define Trap_hxx
#include "Decorator.hxx"


class Trap : public Decorator {
public:
	Trap(AbstractItem *item): Decorator(item){}
	string use(Character &c, Location &l);

};




#endif
