#include <MiniCppUnit.hxx>
#include "Location.hxx"
class LocationTest : public TestFixture< LocationTest >
{
public:
	TEST_FIXTURE( LocationTest )
	{
		TEST_CASE( test_name_byDefault );
		TEST_CASE( test_name_whenChanged );
		TEST_CASE( test_description_whenEmpty );
		TEST_CASE(test_connections_whenConnectedAtSouth);
		TEST_CASE( test_connections_whenNotConnected );
		TEST_CASE(test_connections_whenConnectedAtNorth);
		TEST_CASE(test_description_whenConnectedNorthSouth);
	}

	void test_name_byDefault()
	{
		Location location;
		ASSERT_EQUALS(
			"unknown",
			location.name()
		);
	}
	void test_name_whenChanged()
	{
		Location location;
		location.name( "A location" );
		ASSERT_EQUALS(
			"A location",
			location.name()
		);
	}
	void test_description_whenEmpty()
	{
		Location location;
		location.name( "A location" );
		ASSERT_EQUALS(
			"Location: A location\n",
			location.description()
		);
	}
	void test_connections_whenConnectedAtSouth()
	{
		Location location ;
		location.name( "Madrid" );
		Location locationn ;
		locationn.name( "Cadiz" );
		location.connectSouth( locationn );
		
		ASSERT_EQUALS(
		//"Location: Madrid\n"
		"\tSouth: Cadiz\n",
			//location.description()
			location.connections()
			);
		}
	void test_connections_whenNotConnected()
	{
		Location location ;
		location.name( "Madrid" );
		Location locationn ;
		locationn.name( "cadiz" );
		
		ASSERT_EQUALS(
			"Location: Madrid\n",
			location.description()
		);
	}
	void test_connections_whenConnectedAtNorth()
	{	
		Location location ;
		location.name( "Madrid" );
		Location locationn ;
		locationn.name( "Sevilla" );
		location.connectSouth(locationn);
		
		ASSERT_EQUALS(
			"\tNorth: Madrid\n",
			locationn.connections()
		);
	}
	void test_description_whenConnectedNorthSouth(){
		Location location ;
		location.name( "Madrid" );
		Location locationn ;
		locationn.name( "Bilbao" );
		location.connectSouth(locationn);
		ASSERT_EQUALS(
		    "Location: Bilbao\n"
			"\tNorth: Madrid\n",
			locationn.description()
		);
	}
	
};

REGISTER_FIXTURE( LocationTest )

