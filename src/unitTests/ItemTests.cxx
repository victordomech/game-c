#include <MiniCppUnit.hxx>
#include "Item.hxx"
class ItemTest : public TestFixture< ItemTest >
{
public:
	TEST_FIXTURE( ItemTest )
	{
		TEST_CASE( test_name_byDefault );
		TEST_CASE( test_name_whenChanged );
		TEST_CASE( test_level_byDefault );
		TEST_CASE( test_level_whenChanged );
		
	}

	void test_name_byDefault()
	{
		Item item;
		ASSERT_EQUALS(
			"unknown",
			item.name()
		);
	}

	void test_name_whenChanged()
	{
		Item item;
		item.name( "Hammer" );
		ASSERT_EQUALS(
			"Hammer",
			item.name()
		);
	}
	void test_level_byDefault()
	{
		Item item;
		ASSERT_EQUALS(
			0u,
			item.level()
		);
	}
	void test_level_whenChanged()
	{
		Item item;
		item.level( 40 );
		ASSERT_EQUALS(
			40u,
			item.level()
		);
	}
};

REGISTER_FIXTURE( ItemTest )

