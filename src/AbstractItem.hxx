#ifndef AbstracItem_hxx
#define AbstracItem_hxx
#include <string>

using namespace std;



class Character;
class Location;

class AbstractItem{
	
	public:
	
	virtual string name() const = 0;
	
	virtual void name( string n) = 0;
	
	virtual  int level() const = 0;
	
	virtual void level(unsigned int level) =  0;
	
	virtual bool itsEnable() const = 0;
	
	virtual void enable(bool itsEnable) = 0;
	
	virtual string description() = 0;
	
	virtual string receiveMagic(unsigned int i)=0;
	
	virtual string use(Character &c, Location &l)=0;
	
	virtual ~AbstractItem(){}	
	

};
#endif

