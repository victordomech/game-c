#ifndef Decorator_hxx
#define Decorator_hxx
#include "AbstractItem.hxx"
using namespace std;

class Decorator: public AbstractItem{
	private:
			AbstractItem *_ai;

	public:
		Decorator(AbstractItem *ai){
			_ai=ai;
		}
		~Decorator(){
				if(_ai!=NULL) {
					delete (_ai);
				}
		 }
		string name()const{
			return _ai->name();
		}

		void name(string n){
			 _ai->name( n );
		}
		
		int level()const{
			return _ai->level();
		}
		
		void level( unsigned level){
			 _ai->level(level);
		}
		
		 bool itsEnable()const{
			return _ai->itsEnable();
		 }
		 
		void enable(bool itsEnable){
			_ai-> itsEnable();
		}
		
		string description(){
			return _ai->description();
		 }
		 
		 string receiveMagic(unsigned int i){
			return _ai->receiveMagic(i);
		 }

		 
		string use(Character &c, Location &l){
			return _ai->use(c,l);
		}         
};
#endif
