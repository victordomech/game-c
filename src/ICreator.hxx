#ifndef ICreator_hxx
#define ICreator_hxx
#include "AbstractItem.hxx"
using namespace std;


class ICreator{

	public:
			ICreator(){}
			virtual ~ICreator(){}
			virtual AbstractItem* factorymethod( const string & name, unsigned int level) = 0;
			
};
#endif
