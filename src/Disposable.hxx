#ifndef Disposable_hxx
#define Disposable_hxx
#include "Decorator.hxx"

class Disposable : public Decorator {
	 public:
	 	Disposable(AbstractItem *ai): Decorator(ai){}
	 	string use(Character &character, Location &Location);
};
#endif
