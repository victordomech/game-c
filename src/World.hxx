#include <string>
#include <iostream>
#include <sstream>
#include <list>
#include <map>
#include <fstream>

#include "Item.hxx"
#include "Exception.hxx"
#include "Character.hxx"
#include "Location.hxx"

#include "CCCreator.hxx"
#include "CCreator.hxx"
#include "CreatorC.hxx"
#include "CDCreator.hxx"


#ifndef World_hxx
#define World_hxx
using namespace std;

	class World{
	private :
    list<Location*> _locations;
    list<Character*> _characters;
    map<string, CreatorC*> _creatorsC;
public:

	//CREADORES
	
    World(){
		_creatorsC[ "Cure" ] = new CCCreator();
		_creatorsC[ "Basic" ] = new CCreator();
		_creatorsC[ "Damage" ] = new CDCreator();
		
    }
    
    ~World(){
		for(list<Location *>::iterator it =_locations.begin(); it!=_locations.end(); it++){
			delete(*it);
		}
	
	
		for(list<Character *>::iterator it =_characters.begin(); it!=_characters.end(); it++){
			delete(*it);
		}
		
		for( map<string, CreatorC*>::iterator i= _creatorsC.begin(); i != _creatorsC.end(); i++){
			delete(i->second);
		}
		
	} 
	
	 //CONSULTORES
	 
	 string locations()
    {         
		stringstream ss;	
		
		for(list<Location*>::iterator it = _locations.begin(); it != _locations.end(); ++it){
			if(*it == NULL) 
				ss <<"";

			ss<<(*it)->name()<<"\n";
		}
		return ss.str();	
	}    
	
	 string locationDetails( string name ){
		 for(list<Location*>::iterator it = _locations.begin() ;it != _locations.end(); ++it){
		  if((*it)->name() == name) return (*it)->description();
		  }
		  throw LocationNotFound();
		
	}	
		string characters(){ 
		stringstream ss; 
		for(list<Character *>::iterator it = _characters.begin(); it != _characters.end(); ++it){ 
			if(*it == NULL) ss << ""; 
			ss<<(*it)->name()<<"\n";
			} 
		return ss.str(); 
	}
	Character& findCharacter(string n) const{
		for(list<Character*>::const_iterator i= _characters.begin();i!=_characters.end();i++){
			if ((*i)->name()==n) return (**i);
		}
		throw CharacterNotFound();
	}
	Location& findLocation(string l) const{
		for(list<Location*>::const_iterator i= _locations.begin();i!=_locations.end();i++){
			if ((*i)->name()==l) return (**i);
		}
		throw LocationNotFound();
	}

	  //MODIFICADORES
	  
	 void addLocation( string name ){
		Location* location = new Location();
		location->name(name); 
		_locations.push_back(location);
	} 
	
	string addItemAtLocation( string locat, string name , int requiredLevel){
		stringstream ss;	
		 for(list<Location*>::iterator it = _locations.begin() ;it != _locations.end(); ++it){
			if((*it)->name() == locat) 
			  (*it)->addItem(name,requiredLevel);
			  ss <<"";
			  return ss.str();	
		}               
		 throw LocationNotFound();
	 }
	 	 
	 //METODOS
	 	 	 
	void connectNorthToSouth(const string &name_N, const string &name_S){
		
		for(list<Location*>::iterator it = _locations.begin() ;it != _locations.end(); ++it) {
			if ((*it)->name() == name_N){
				for(list<Location*>::iterator it2 = _locations.begin(); it2 != _locations.end(); ++it2) {
					if ((*it2)->name() == name_S)
							(*it)->connectSouth(*(*it2));
				}
			}
		}
	}
	void connectWestToEast(const string &name_E, const string &name_W){
		
		for(list<Location*>::iterator it = _locations.begin() ;it != _locations.end(); ++it) {
			if ((*it)->name() == name_E){
				for(list<Location*>::iterator it2 = _locations.begin(); it2 != _locations.end(); ++it2) {
					if ((*it2)->name() == name_W)
							(*it)->connectEast(*(*it2));
				}
			}
		}
	}
	void addCharacter(string name,unsigned int level){

		Character* character= new Character();	
		//_creators[type]FactoryMethod{}	
		character->name(name);
		character->level(level);
		_characters.push_back(character);
	}
	int placeCharacter(string character_name, string location_name){
		Character &character= findCharacter(character_name);
		Location &location = findLocation(location_name);
		character.locateAt(location);
		return 0;
		}
	
	string useItem(string loc, string cha, string item){
		Location &location = findLocation(loc);
		findCharacter(cha);
		return location.useItem(cha, item);
	}
	
	string distributeMagic(string loc, unsigned int i){
		Location &location = findLocation(loc);
		return location.distributeMagic(i);
	}
	
	void addDamageCharacter(string name, unsigned level){
		Character* cha=new DamageCharacter();
		
		cha->name(name);
		cha->level(level);
		_characters.push_back(cha);
	}
	
	void addTrapAtLocation(string loc, string item, unsigned int level){
		Location &location = findLocation(loc);
		location.addTrap(item, level);
	}
	
	void addCureCharacter(string name, unsigned level){
		Character *cha=new CureCharacter();
		cha->name(name);
		cha->level(level);
		_characters.push_back(cha);
	}
	
	
	void addCharacterCreator( const string & name, const string & type, unsigned int level){
		_characters.push_back( _creatorsC[ type ]->factorymethod(name,level) );
	}
	
	 
	void addPotionAtLocation(string loc, string item, unsigned int level){
		Location &location = findLocation(loc);
		location.addPotion(item, level);
	}
	
	void addBombAtLocation(string loc, string item, unsigned int level){
		Location &location = findLocation(loc);
		location.addBomb(item, level);
	}
	
	void loadMap( string file){
		ifstream fichero(file.c_str());
		string line;
		while(fichero){
			getline( fichero, line);
			string token;
			istringstream is( line );
			string type;
			is >>type;
			
			if( type=="Item"){
				string type2, name, locat;
				int level;
				is>> type2 >>name>> level >> locat;
				
				if((type2=="")||(name=="") ||(locat=="")) throw IncorrectMap();
				
				else if( type2 !="Bomb" && type2 !="Trap" && type2 !="Potion" && type2 !="Basic") throw InvalidType();
				
				findLocation(locat).addItemCreator(name,type2, level);
				
			}
			
			else if ( type=="Location"){
				string name;
				is>> name;
				addLocation(name);
			} 
			else if (type=="Character" ){
				string name, cha;
				unsigned int level;
				is>> cha >> name >> level;
				
				if ( cha != "Basic" && cha != "Damage" && cha != "Cure"){
					throw InvalidType();
			}
				addCharacterCreator(name ,cha, level );
			
			}
			else if (type== "LocateAt" ){
				string cha, loc;
				
				is>> cha >> loc;
				placeCharacter(cha, loc);
			}
			else if(type== "Connection" ){
				string cardinality, origen, destino;
				is>> cardinality>> origen>> destino;
				
				if( cardinality == "North")
				connectNorthToSouth ( origen, destino );
				else
				connectWestToEast(origen, destino);
			}
		}
	}
	
	string LocCha( string character){
		Character cha = findCharacter( character);
		for(list<Location*>::const_iterator i= _locations.begin();i!=_locations.end();i++){
			Character cha2 = (*i)->findCharacter(character);
			if (cha2.name()== cha.name()) return ((*i)->name());
		}
		throw LocationNotFound();
	}
		
	
	string pickup ( string cha, string weapon){
			Character &character= findCharacter(cha);
			character.pickup(weapon);
			return cha + " picks up " + weapon + "\n";
	}
		
	string setMainWeapon(string cha, string weapon){		
		Character &character= findCharacter(cha);
		character.setMainWeapon(weapon);
		return cha+" set "+ weapon + " as main weapon\n";	
			 throw ItemNotFound();
	}
	
	string hit(string ch1, string ch2){
		Character &cha1= findCharacter(ch1);
		Character &cha2= findCharacter(ch2);
		if (cha1._location!=cha2._location){
			throw CharacterNotFound();
		}
		else if(cha1._we==NULL){
			cha1.golpe();
		}
		return cha1.hit(cha2);
	}	
	void move( string cha, string loc ){
		Character &cha1= findCharacter(cha);
		cha1.move( loc );
		//findCharacter(cha)->move(loc);
	}
	 
};

#endif
