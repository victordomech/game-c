#ifndef Location_hxx
#define Location_hxx
#include <string>
#include <iostream>
#include <sstream>
#include <list>
#include <map>

#include "Item.hxx"
#include "Exception.hxx"
#include "Character.hxx"
#include "AbstractItem.hxx"
#include "DamageCharacter.hxx"
#include "Disposable.hxx"
#include "Trap.hxx"
#include "CureCharacter.hxx"
#include "Potion.hxx"
#include "Bomb.hxx"

#include "IBombCreator.hxx"
#include "IPotionCreator.hxx"
#include "ITrapCreator.hxx"
#include "IBasicCreator.hxx"
#include "ICreator.hxx"


using namespace std;
class Location{
    private :
    string _name;
    //Item item;
    list<AbstractItem*> _item;
    list<Character*> _characters;
    Location *South;
    Location *North;
    Location *East;
    Location *West;
    
    map<string, ICreator*> _icreator;
    
public:

	//CREADORES
Location (){
    _name="unknown";
	South=NULL;
	North=NULL;
	East=NULL;
	West=NULL;
	
	_icreator[ "Trap" ] = new ITrapCreator();
	_icreator[ "Bomb" ] = new IBombCreator();
	_icreator[ "Basic" ] = new IBasicCreator();
	_icreator[ "Potion" ] = new IPotionCreator();
    }
    
    
    ~Location(){
		for(list<AbstractItem *>::iterator it =_item.begin(); it!=_item.end(); it++){
			delete(*it);
		}
		
		for (map<string, ICreator*>::iterator i= _icreator.begin(); i !=_icreator.end(); i++){
			delete(i->second);
		}
	}
    
    //CONSULTORES
    
    string name()const{
        return _name;
    }
    
    string description()
    {
		return "Location: " + _name +"\n"+ this->connections()+ this->items()+ this->characters();	
	}
	
    string items(){
		
		stringstream ss;	
		for(list<AbstractItem*>::iterator it = _item.begin(); it != _item.end(); ++it){
			if(*it == NULL) 
				ss <<"";

			ss<<"\tItem: "<<(*it)->name()<<" ["<<(*it)->level()<<"]\n";
		}
		return ss.str();	
	}
	
	string characters(){ 
		stringstream ss; 

		for(list<Character *>::iterator it = _characters.begin(); it != _characters.end(); ++it){ 
			if(*it == NULL) ss << ""; 
			else ss <<"- "<< (*it)->name() << " is here.\n";
			} 
		return ss.str(); 
	}
    
    
     string connections()const{
		  stringstream ss;
		  
		  if (North!=NULL){
		  ss <<"\tNorth: "<< North->name()<<"\n";
		  }
		  if (South!=NULL){
		  ss <<"\tSouth: "<< South->name()<<"\n";
		  }
		  if (East!=NULL){
		  ss <<"\tEast: "<< East->name()<<"\n";
		  }
		  if (West!=NULL){
		  ss <<"\tWest: "<< West->name()<<"\n";
	      }	
		  	 
		  return ss.str();
	  } 
	  
	  
    //MODIFICADORES
    
    void name( const string &name)
     {
        _name= name; 
     }
     
      void connectSouth (Location &name)
	 {
		  South = &name ;
		  this->South->North=this;

	 }
	 void connectEast (Location &name)
	 {
		  East = &name ;
		  this->East->West=this;

	 }
	void addItem(string name,unsigned int level){
		
		AbstractItem* item = new Item();		
		item->name(name);
		item->level(level);
		_item.push_back(item);
	}
	
	 //METODOS
	 
	 AbstractItem & findItem(string name){ 
		 for(list<AbstractItem*>::iterator it = _item.begin() ;it != _item.end(); ++it){
		  if((*it)->name() == name) 
			  return *(*it);
		}
		throw ItemNotFound();
	}
	void placeCharacter( Character &character ){ 

		_characters.push_back(&character);
	} 
	
	void unplaceCharacter( Character &character ){
		for(list<Character*>::iterator it = _characters.begin() ;it != _characters.end(); ++it){
			if((*it)->name() == character.name()) {
				_characters.erase(it);
				it = _characters.end();
			}
		 }
	}
	
	Character & findCharacter(string name){ 
		 for(list<Character*>::iterator it = _characters.begin() ;it != _characters.end(); ++it){
		  if((*it)->name() == name) 
			  return *(*it);
		  }
		  throw ExceptionCharacter();
	  }
	 
	 
	string useItem(string cha, string itemm ){
		stringstream ss;
		AbstractItem &item = findItem(itemm);
		Character &character= findCharacter(cha);
		
		if (character.level()<item.level()) {
			ss<< "The level of " <<character.name()<<" is too low\n";
		}
		else{
			if (item.itsEnable()) {
				ss <<character.name()<<" uses "<<item.name()<<" at "<<name()<<"\n";
				ss << item.use(character, *this);
			}else{
				ss<<item.name() << " not enabled\n";
				}
			
			}
			return ss.str();
		
	}
	
	string distributeMagic(unsigned int i){
		stringstream ss;
		
		for(list<Character*>::iterator it = _characters.begin() ;it != _characters.end(); ++it){
			ss << (*it)->receiveMagic(i);
		}

		for(list<AbstractItem*>::iterator it = _item.begin() ;it != _item.end(); ++it){
			
			ss<<(*it)->receiveMagic(i);
		}
		return ss.str();
	}
	
	void addTrap(string name, unsigned int level){
		AbstractItem* item = new Disposable(new Trap(new Item()));
		item->name(name);
		item->level(level);
		item->enable(1);
		_item.push_back(item);
	}
	void removeItem(string item)
	{
		for(list<AbstractItem*>::iterator it = _item.begin() ;it != _item.end(); ++it){
			if((*it)->name()==item){
				delete(*it);
				it=_item.erase(it);	
			}
			
		}
	}
	
	void addPotion(string name, unsigned int level){
		AbstractItem* item =new Disposable(new Potion(new Item()));
		item->name(name);
		item->level(level);
		item->enable(1);
		_item.push_back(item);
	}
	
	void addBomb(string name, unsigned int level){
		AbstractItem* item =new Disposable(new Bomb(new Item()));
		item->name(name);
		item->level(level);
		//item->enable();
		_item.push_back(item);
	}
			
	void addItemCreator( string name, string type, unsigned int level){
		_item.push_back(_icreator[type]->factorymethod(name, level) );
	}
	void unplaceItem(string i){
		AbstractItem &item = findItem(i);
		_item.remove(&findItem(item.name()));
	}
	
	
	Location* getWest(){
		return West;
	}
	Location* getEast(){
		return East;
	}
	Location* getNorth(){
		return North;
	}
	Location* getSouth(){
		return South;
	}
		
		   
};
#endif

