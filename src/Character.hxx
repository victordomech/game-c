#ifndef Character_hxx
#define Character_hxx

#include <sstream>
#include <list>

#include "AbstractWeapon.hxx"

using namespace std;

class Location;
class AbstractItem;

class Character{
    private :
 	 string _name;
	 int _level;
	 int _life;
	// Location* _location;
	 list <AbstractItem*> _item;
	 
	 
public:
		
  Location* _location;
	//CREADORES
  AbstractWeapon *_we;
  
  Character() {
    _name= "unknown" ;
    _level=0;
    _life=10;
    _location = NULL;
    _we = NULL;
    }
    virtual ~Character();
    
        //CONSULTORES	  
  string name(){
        return _name;
    }
    
     int level()const{
	return _level;
  }

	int life(){
	return _life;
  }
  
  virtual string damage(int damage)
{	stringstream ss;
	if(damage<=_life) _life -= damage;		
	else
	_life= 0;
	ss << _name << " takes " << damage << " damage\n";
	return ss.str();

}

    //MODIFICADORES
    
	void name( const string name)
	{
        _name= name;
    }


	void level( unsigned level)
	{
	_level= level;
	}


	void life( int lif)
	{
	_life= lif;
	}

	void locateAt(Location & _location);
	
	virtual string receiveMagic(unsigned int i){
			stringstream ss;
			ss << name() << " receives " << i <<" magic points\n";
			return ss.str();
		}
	virtual string cure(unsigned int cure){
			stringstream ss;
			if (cure == 1){
				_life=10;
			}
			else{
				if((cure+_life)>10){
					_life=10;
				}
				else _life+=cure;
			}
			ss << _name << " gains " << cure << " life\n";
			return ss.str();

		}
		
		void pickup(string weapon);
		
		void setMainWeapon(string weapon);
		
		std::string hit (Character &cha){
			
			return _we->hit(cha);
			
			
		}
		/*
		void move( string loc){
			
			if(_location != NULL){
				
				if (loc=="West"){
					if(_location->getWest()== NULL)
						throw LocationNotFound();
					else
					locateAt(_location->getWest();
				}
				if (loc=="East"){
					if(_location->getEast()== NULL)
						throw LocationNotFound();
					else
					locateAt(_location->getEast();
				}
				if (loc=="North"){
					if(_location->getNorth()== NULL)
						throw LocationNotFound();
					else
					locateAt(_location->getNorth();
				}
				if (loc=="South"){
					if(_location->getSouth()== NULL)
						throw LocationNotFound();
					else
					locateAt(_location->getSouth();
				}
			}
			else throw LocationNotFound();
		}
		*/
		void move ( string loc);
		string dropAllItems();
		void golpe ();
		AbstractItem& findItem(string n);		
};
#endif

