#ifndef CDCreator_hxx
#define CDCreator_hxx
#include "CreatorC.hxx"
using namespace std;


class CDCreator: public CreatorC{

public:


	Character* factorymethod(const string &name, unsigned int level){
		Character *cha  = new DamageCharacter();
		
		cha->name( name );
		cha->level( level );
	
		return cha;
	}
};
#endif
