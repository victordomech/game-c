#include "Location.hxx"
#include "Character.hxx"
#include "Hammer.hxx"
#include "Fist.hxx"
#include "Staff.hxx"
#include "Sword.hxx"
#include <sstream>
using namespace std;
Character::~Character(){	
	for(list<AbstractItem*>::iterator i= _item.begin();i!=_item.end();i++){
		delete (*i);
	}
	if (_we){
		delete(_we);
	}
}

AbstractItem& Character:: findItem(string i){
	for(list<AbstractItem*>::const_iterator it= this->_item.begin();it!=this->_item.end();it++){
		if ((*it)->name()==i)return (**it);
	}
	throw ItemNotFound();
}

void Character::locateAt(Location &location){
	if(_location!=NULL)
		_location->unplaceCharacter(*this);
		_location = &location;
		_location->placeCharacter(*this);
}

		
void Character::pickup(string weapon){
	if (_location==NULL) throw ItemNotFound();
	AbstractItem &item =_location->findItem(weapon);
	_item.push_back(&item);
	_location->unplaceItem(weapon);
}

void Character::setMainWeapon(string we){

	findItem(we);
		
	if (we=="Hammer"){
		AbstractWeapon* hammer= new Hammer();
		_we=hammer;
	}
	else if (we=="Staff"){
		AbstractWeapon* staff= new Staff();
		_we=staff;
	}
	else if (we=="Sword"){
		AbstractWeapon* sword= new Sword();
		_we=sword;
	}
	else if( we=="Fist"){
		AbstractWeapon* fist= new Fist();
		_we=fist;
	}
	else if(_we){
		delete(_we);
	}
}

string Character::dropAllItems(){
	stringstream ss;	
	for(list<AbstractItem*>::iterator i= _item.begin();i!=_item.end();i++){
		ss << Character::name() << " drops " <<(*i)->name()<<"\n";
			delete(*i);
		i=_item.erase(i);
	}
	if(_we!=NULL){
		delete(_we);
		_we=new Fist();
		}
	return ss.str();
}

void Character::golpe (){
	AbstractWeapon* fist= new Fist();
	_we=fist;
}

		void Character::move( string loc){
			
			if(_location != NULL){
				
				if (loc=="West"){
					if(_location->getWest()== NULL)
						throw LocationNotFound();
					else
					locateAt(*(_location->getWest()));
				}
				if (loc=="East"){
					if(_location->getEast()== NULL)
						throw LocationNotFound();
					else
					locateAt(*(_location->getEast()));
				}
				if (loc=="North"){
					if(_location->getNorth()== NULL)
						throw LocationNotFound();
					else
					locateAt(*(_location->getNorth()));
				}
				if (loc=="South"){
					if(_location->getSouth()== NULL)
						throw LocationNotFound();
					else
					locateAt(*(_location->getSouth()));
				}
			}
			else throw LocationNotFound();
		}
		

