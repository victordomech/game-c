#ifndef CreatorC_hxx
#define CreatorC_hxx
#include "Character.hxx"
using namespace std;


class CreatorC{

public:
		CreatorC(){}
		virtual ~CreatorC(){}
		virtual Character*factorymethod( const string & name, unsigned int level ) =0;
		

};
#endif
