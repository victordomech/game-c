#ifndef DamageCharacter_hxx
#define DamageCharacter_hxx
#include "Character.hxx"
using namespace std;


class DamageCharacter: public Character{

public:
	DamageCharacter(){}
	~DamageCharacter(){}
	string receiveMagic( unsigned int i){
		stringstream ss;
		ss << name() << " receives " << i <<" magic points\n"<<damage(i);
		return ss.str();
	}
};
#endif
