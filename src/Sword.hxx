#ifndef Sword_hxx
#define Sword_hxx
#include <sstream>
class Sword : public AbstractWeapon {
	 
public:
	Sword (){}
	std::string hit(Character &cha){
		std::stringstream ss;
		ss << cha.damage(4);
		/**IS DEAD**/
		if (cha.life()==0) {
			ss << cha.name() <<" is dead\n";
		}
		return ss.str();
	}
};
#endif
