#include "Location.hxx"
#include "Character.hxx"

string Disposable::use(Character &c, Location &l){
	stringstream ss;
	ss<< Decorator::use(c,l);
	l.removeItem(this->name());
	return ss.str();
}
