#include "Location.hxx"
#include "Character.hxx"

std::string Trap::use(Character &cha, Location &loc){
	
	return Decorator::use(cha,loc) + cha.damage(5);
}
