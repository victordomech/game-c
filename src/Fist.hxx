#ifndef Fist_hxx
#define Fist_hxx
#include <sstream>
class Fist : public AbstractWeapon{	 
public:
	Fist() {}
	std::string hit(Character &c){
		std::stringstream ss;
		ss <<c.damage(1);
		/**IS DEAD**/		
		if (c.life()==0) {
			ss << c.name() <<" is dead\n";
		}
		return ss.str();
	}
};
#endif
