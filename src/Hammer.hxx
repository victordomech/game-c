#ifndef Hammer_hxx
#define Hammer_hxx
#include <sstream>
using namespace std;
class Hammer : public AbstractWeapon{
	 
public:
	Hammer() {}
	std::string hit(Character &cha){
		std::stringstream ss;		
		ss << cha.dropAllItems();
		ss << cha.damage(2);		
		return ss.str();
	}
};

#endif
