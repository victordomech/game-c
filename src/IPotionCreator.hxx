#ifndef IPotionCreator_hxx
#define IPotionCreator_hxx
#include "ICreator.hxx"
using namespace std;


class IPotionCreator: public ICreator{

public:

	AbstractItem* factorymethod(const string & name, unsigned int level ) {
		AbstractItem* _item = new Potion (new Item() );
		
		_item-> name( name );
		_item-> level( level );
		
		return _item;
	}
};
#endif
