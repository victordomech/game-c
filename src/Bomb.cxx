#include "Location.hxx"
#include "Character.hxx"
#include <sstream>
#include <string>
string Bomb::use(Character &c, Location &l){
	stringstream ss;
	
	ss << Decorator::use(c,l);
	ss << "Bomb explodes\n";
	ss << l.distributeMagic(5);
	return ss.str();
}
