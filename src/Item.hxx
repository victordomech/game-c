#ifndef Item_hxx
#define Item_hxx
#include <string>
#include <sstream>
#include "AbstractItem.hxx"

using namespace std;
class Location;
class Character;

class Item: public AbstractItem{
    private :
    string _name;
    int _level;
	bool _enable;
	public:

	//CREADORES
	
    Item(){
    _name="unknown";
    _level=0;
    _enable=true;
    }
    
    
    //CONSULTORES
    
    string name()const{
        return _name;
    }
    
    int level()const
	{
	return _level;
  	}
  	
  	string description(){
			std::stringstream ss;
		
		ss<<_name<<" ["<<_level<<"]";
		
		return ss.str();	
	}
	
	  bool itsEnable()const{
		  return _enable;
    }
	
	//MODIFICADORES
	
   void name( const string name)
	{
        _name= name;
    	}

   void level(unsigned int level)
	{
	_level= level;
	} 
	
   void enable(bool itsEnable){
		_enable = itsEnable;
	}
   
   
   //METODOS
   
   string use(Character &c, Location &l){
		
		return "";
	}
	string receiveMagic(unsigned int i){
		enable(1);
		stringstream ss;
		if(name()=="Bomb")return "";
		ss << name() << " receives " << i <<" magic points\n";
		return ss.str();
	}	
};
#endif
