#ifndef Bomb_hxx
#define Bomb_hxx
#include "Decorator.hxx"

class Bomb : public Decorator {
	 
public:
	Bomb(AbstractItem *item): Decorator(item){
		item->enable(0);}
	
	string use(Character &c, Location &l);
};
#endif
