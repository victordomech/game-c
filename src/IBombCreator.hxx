#ifndef IBombCreator_hxx
#define IBombCreator_hxx
#include "ICreator.hxx"
using namespace std;


class IBombCreator: public ICreator{

public:

	AbstractItem* factorymethod(const string & name, unsigned int level ) {
		AbstractItem* _item =new Bomb (new Item() );
		
		_item-> name( name );
		_item-> level( level );
		
		return _item;
	}
};
#endif
