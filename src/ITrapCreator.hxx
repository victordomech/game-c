#ifndef ITrapCreator_hxx
#define ITrapCreator_hxx
#include "ICreator.hxx"
using namespace std;


class ITrapCreator: public ICreator{

public:

	AbstractItem* factorymethod(const string & name, unsigned int level ) {
		AbstractItem* _item = new Trap (new Item() );
		
		_item-> name( name );
		_item-> level( level );
		
		return _item;
	}
};
#endif
