#ifndef CureCharacter_hxx
#define CureCharacter_hxx
#include <string>
#include "Character.hxx"
using namespace std;


class CureCharacter: public Character{

	public:
	CureCharacter(){}
	~CureCharacter(){}

	string receiveMagic(unsigned int i){
		stringstream ss;
		
		ss << name() << " receives " << i <<" magic points\n" << cure(i);
		return ss.str();
	}
};
#endif
