#ifndef IBasicCreator_hxx
#define IBasicCreator_hxx
#include "ICreator.hxx"
using namespace std;


class IBasicCreator: public ICreator{

public:

	AbstractItem* factorymethod(const string & name, unsigned int level ) {
		AbstractItem* _item = new Item() ;
		
		_item-> name( name );
		_item-> level( level );
		
		return _item;
	
	}
};
#endif
