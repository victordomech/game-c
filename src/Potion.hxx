#ifndef Potion_hxx
#define Potion_hxx
#include "Decorator.hxx"

class Potion : public Decorator {
	 
public:
	Potion(AbstractItem *item): Decorator(item){}
	
	string use(Character &c, Location &l);
};
#endif
