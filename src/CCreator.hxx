#ifndef CCreator_hxx
#define CCreator_hxx
#include "CreatorC.hxx"
using namespace std;


class CCreator: public CreatorC{

public:

	Character* factorymethod(const string &name, unsigned int level){
		Character *cha  = new Character();
		
		cha->name( name );
		cha->level( level );
	
		return cha;
	}
};
#endif
