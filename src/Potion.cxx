#include "Location.hxx"
#include "Character.hxx"
#include <sstream>

string Potion::use(Character &c, Location &l){
	return Decorator::use(c,l) + c.cure(5);
}
