//Joseph Valdivia NIA: 165914
//Victor Domenech NIA: 172888

#include <list> 
#include "animals.hxx"
#include "Jaulas.hxx"
using namespace std;
typedef list<Jaulas*> ListdeJaulas; 

int main(void) {
	
	Jaulas jaulaPokemon, jaulaMamiferos, estancoSapos, jaulaFelinos;
	/***************Nombre de los recintos***************/
	jaulaPokemon.setJaulas("Recinto  Pokemon");
	jaulaMamiferos.setJaulas("Recinto de Elefantes");
	estancoSapos.setJaulas("Estanco  de sapos");
	jaulaFelinos.setJaulas("Recinto de Tigres");

	ListdeJaulas jaula;
	/***************Pokemon***************/
	Animal unAnimal, dosAnimal;
	unAnimal.nom(" Pikatxu");
	jaulaPokemon.setAnimales(&unAnimal);
	dosAnimal.nom(" Raichu");
	jaulaPokemon.setAnimales(&dosAnimal);
	
	jaula.push_back(&jaulaPokemon);

	/***************Elefantes***************/	
	Elefant unElefant, dosElefant;
	unElefant.nom(" Elefante macho");
	jaulaMamiferos.setAnimales(&unElefant);
	
	dosElefant.nom(" Elefante hembra");
	jaulaMamiferos.setAnimales(&dosElefant);
	
	jaula.push_back(&jaulaMamiferos);
	/***************Sapos***************/
	Granota unaGranota,dosGranota;
	unaGranota.nom(" Sapo");
	estancoSapos.setAnimales(&unaGranota);
	dosGranota.nom(" Rana");
	estancoSapos.setAnimales(&dosGranota);
	
	jaula.push_back(&estancoSapos);
	
	/***************Tigres***************/
	Gat unGat, dosGat;
	unGat.nom(" Tigre macho");
	jaulaFelinos.setAnimales(&unGat);
	dosGat.nom(" Tigre hembra");
	jaulaFelinos.setAnimales(&dosGat);
	
	jaula.push_back(&jaulaFelinos);

	/***************iterem fent servir iteradors***************/
	for (ListdeJaulas::iterator it=jaula.begin(); it!=jaula.end(); it++)
	{
		cout << "En el " << (*it)->getJaulas() << " estan los animales: \n";
		(*it)->listAnimalsToString();
	}
	return 0;
}
