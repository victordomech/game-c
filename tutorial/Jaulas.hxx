#ifndef jaulas_hxx 
#define jaulas_hxx
using namespace std;

class Jaulas{
public:
	void setJaulas(string jaula){
		_jaula = jaula;
	}

	string getJaulas(){
		return _jaula;
	}

	void setAnimales(Animal *animal){
		animals.push_back(&(*animal));
	}

	void listAnimalsToString(){
		for (list<Animal*>::iterator it=animals.begin(); it!=animals.end(); it++)
		{
			cout << "\t-->El animal dentro de la jaula es:" << (*it)->nom() << "\n";
		}
	}

private:
	string _jaula;
	list<Animal*> animals;
};


#endif
