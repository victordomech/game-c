 //complex code :
 

#include <iostream>
using namespace std;

template <typename num>

 class complex{
 
 	private:
 		num _real;
 		num _imaginary;
 
  	public:	 
 		complex(num real, num img):_real(real), _imaginary(img){}
  
  		num Real() const { return _real; }
  		num Img() const { return _imaginary; }
  
  		complex operator+(const complex & other() ){
 		 	return complex(_real +other.Real(), _imaginary + other.Img());
  		}
  	
  		complex operator * (const complex & other() ){
   			return complex((_real*other.Real())-(_real*other.Real()),(_imaginary*other.Img())+(_imaginary*other.Img())) ;
   		}
  	
  		complex operator+(const num  other){
   			return complex(_real + other, _imaginary);
   		}
   
  		complex operator*(const num other() ){
  			return complex(_real * other ,_imaginary*other);
  		}
  	
  };
